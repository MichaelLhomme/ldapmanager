/* eslint-disable @typescript-eslint/no-explicit-any */

import { Change } from 'ldapjs'

import { Connection } from 'LDAP/Connections'
import { ldapModify } from './Primitives'


/**
 * LDAP modify operations
 *
 * https://ldapwiki.com/wiki/Modify%20Request
 */
export enum ChangeType { // eslint-disable-line no-shadow
  Add = 'add',
  Replace = 'replace',
  Delete = 'delete'
}


/**
 * Helper class for ldap modify requests
 */
export default class ChangeSet {

  // Internal array of LDAP change
  private changes: Change[]

  // Constructor
  public constructor() { this.changes = [] }

  // Helper adding new LDAP change object
  private createChange(type: ChangeType, attr: string, value?: any): ChangeSet {
    this.changes.push(new Change({
      operation: type as string,
      modification: { [attr]: value }
    }))

    return this
  }

  // Execute the modify request
  public execute(conn: Connection, dn: string): Promise<void> {
    return ldapModify(conn, dn, this.changes)
  }

  // Merge another change set
  public merge(cs: ChangeSet): ChangeSet {
    this.changes = this.changes.concat(cs.changes)
    return this
  }

  // Add a new add operation
  public add(attr: string, value: any): ChangeSet {
    this.createChange(ChangeType.Add, attr, value)
    return this
  }

  // Add a new delete operation
  public delete(attr: string, value?: any): ChangeSet {
    this.createChange(ChangeType.Delete, attr, value)
    return this
  }

  // Add a new replace operation
  public replace(attr: string, value?: any): ChangeSet {
    this.createChange(ChangeType.Replace, attr, value)
    return this
  }

}
