FROM node
COPY ./ /home/node
WORKDIR /home/node

RUN yarn \
  && yarn prod:build \
  && yarn test

CMD yarn prod:run
