import dotenv from 'dotenv'

import Server from 'REST/Server'
import { ConnectionPool } from 'LDAP/Connections'

// Configure dotenv
dotenv.config()

// Start internal connection pool
const pool: ConnectionPool = new ConnectionPool()

// Setup restify server
const server: Server = new Server(pool)
server.start()
