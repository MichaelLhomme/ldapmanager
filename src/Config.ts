/* eslint-disable @typescript-eslint/no-explicit-any */
export function config(key: string, defaultValue?: any | undefined) : any {
  const value = process.env[key]

  if (value) return value
  if (defaultValue) return defaultValue
  throw new Error(`Missing configuration key '${key}'`)
}

export function configInt(key: string, defaultValue?: number | undefined) : number {
  return parseInt(config(key, defaultValue))
}
