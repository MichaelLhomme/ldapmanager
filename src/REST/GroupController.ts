import restify from 'restify'

import { createLogger } from 'Logger'
import { Query, ldapCreate, ldapDelete } from 'LDAP/Requests'
import { Connection, ConnectionPool } from 'LDAP/Connections'

import { Group } from 'DTO/Group'

import Controller, { Request } from './Controller'

const log = createLogger({ base: 'GroupController' })


/**
 * Interface describing the query for OUs filtering
 */
export interface GroupQuery {
  name?: string
}


/**
 * Interface describing the query for creating an OU
 */
export interface GroupCreate {
  name?: string,
  description?: string
}


/**
 * OU controller
 */
export default class OUController extends Controller {

  /**
   * Create a new OUController instance
   */
  constructor(server: restify.Server, pool: ConnectionPool) {
    super(server, pool)

    this.get('/groups', this.searchGroup)
    this.post('/groups', this.createGroup)
    this.get('/groups/:groupDN', this.getGroup)
    this.put('/groups/:groupDN', this.updateGroup)
    this.del('/groups/:groupDN', this.deleteGroup)
  }


  /**
   * Handle search
   *
   * Search parameters are defined in interface `GroupQuery`
   */
  private async searchGroup(req: Request, res: restify.Response, next: restify.Next): Promise<void> {
    try {
      // Process query parameters into filters
      const q: Query.Builder = new Query.Builder((item: Query.Item) => Group.fromLDAP(item))
        .objectClass('organizationalUnit')
        .searchDN('ou=users', true)

      const searchParams: GroupQuery = req.params
      if (searchParams.name) q.attr('ou', `*${searchParams.name}*`)

      // Execute the query
      const conn: Connection = this.getUserConnection(req)
      const searchResults: Group[] = await q.build().search(conn)
      res.send(searchResults)
      next()
    } catch (err) {
      log.error({ msg: 'Error searching for ous', err })
      next(new Error(`Error searching for ous : ${err}`))
    }
  }


  /**
   * Handle get group
   */
  private async getGroup(req: Request, res: restify.Response, next: restify.Next): Promise<void> {
    try {
      const { groupDN } = req.params
      if (!groupDN || groupDN.length === 0) throw 'missing DN'

      // Execute the search request
      const q: Query<Group> = new Query.Builder((item: Query.Item) => Group.fromLDAP(item))
        .recurse(false)
        .searchDN(groupDN)
        .objectClass('organizationalUnit')
        .build()

      const conn: Connection = this.getUserConnection(req)
      const searchResults: Group[] = await q.search(conn)
      if (searchResults.length !== 1) throw `error, only one user expected, got ${searchResults.length}`
      res.send(searchResults[0])
      next()
    } catch (err) {
      log.error({ msg: 'Error getting group', err })
      next(new Error(`Error getting group: ${err}`))
    }
  }



  /**
   * Handle update group
   */
  private async updateGroup(req: Request, res: restify.Response, next: restify.Next): Promise<void> {
    try {
      const { groupDN } = req.params
      if (!groupDN || groupDN.length === 0) throw 'missing DN'

      // Extract request body
      const { description }: { description: string | undefined } = req.body
      const updater = Group.update()
      if (description) updater.setDescription(description)

      // Execute the search request
      const conn: Connection = this.getUserConnection(req)
      await updater.execute(conn, groupDN)

      log.info(`Group '${groupDN}' updated`)
      res.send(updater.getUpdates())
      next()
    } catch (err) {
      log.error({ msg: 'Error updating group', err })
      next(new Error(`Error updating group: ${err}`))
    }
  }



  /**
   * Handle create
   */
  private async createGroup(req: Request, res: restify.Response, next: restify.Next): Promise<void> {
    try {
      // Validate and extract the OU schema
      if (!req.body.parent) throw 'missing "parent" property'
      const { parent } = req.body

      const group: Group = Group.fromQuery(req.body)
      group.id = `ou=${group.name},${parent}`

      // Execute the create request
      const conn: Connection = this.getUserConnection(req)
      await ldapCreate(conn, group.id, group.toLDAP())

      log.info(`New group '${group.id}' created`)
      res.send(group)
      next()
    } catch (err) {
      log.error({ msg: 'Error creating group', err })
      next(new Error(`Error creating group: ${err}`))
    }
  }


  /**
   * Handle delete
   */
  private async deleteGroup(req: Request, res: restify.Response, next: restify.Next): Promise<void> {
    try {
      // Validate and extract query data
      const { groupDN } = req.params
      if (!groupDN || groupDN.length === 0) throw 'missing group DN'

      // Execute the delete request
      const conn: Connection = this.getUserConnection(req)
      await ldapDelete(conn, groupDN)

      log.info(`Group '${groupDN}' deleted`)
      res.send()
      next()
    } catch (err) {
      log.error({ msg: 'Error deleting group', err })
      next(new Error(`Error deleting group: ${err}`))
    }
  }

}
