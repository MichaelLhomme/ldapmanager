/* eslint-disable max-len */

import restify from 'restify'

import { Connection, ConnectionPool } from 'LDAP/Connections'


/**
 * Interface describing the JWT payload
 */
export interface TokenPayload {
  dn: string,
  iat: number,
  exp: number
}


/**
 * Interface describing an HTTP Request extended with user
 * information and its associated LDAP connection
 */
export interface Request extends restify.Request {
  user?: TokenPayload,
  conn?: Connection,
}


/**
 * Base class for controllers
 *
 * Provide access to the connection pool and helpers to register routes
 */
export default class Controller {
  protected server: restify.Server

  protected pool: ConnectionPool

  // Store the application components
  constructor(server: restify.Server, pool: ConnectionPool) {
    this.server = server
    this.pool = pool
  }

  // Helper to register a GET route
  protected get(route: string, callback: (req: Request, res: restify.Response, next: restify.Next) => void) {
    this.server.get(route, (req: Request, res: restify.Response, next: restify.Next) => callback.bind(this)(req, res, next))
  }

  // Helper to register a POST route
  protected post(route: string, callback: (req: Request, res: restify.Response, next: restify.Next) => void) {
    this.server.post(route, (req: Request, res: restify.Response, next: restify.Next) => callback.bind(this)(req, res, next))
  }

  // Helper to register a PUT route
  protected put(route: string, callback: (req: Request, res: restify.Response, next: restify.Next) => void) {
    this.server.put(route, (req: Request, res: restify.Response, next: restify.Next) => callback.bind(this)(req, res, next))
  }

  // Helper to register a DELETE route
  protected del(route: string, callback: (req: Request, res: restify.Response, next: restify.Next) => void) {
    this.server.del(route, (req: Request, res: restify.Response, next: restify.Next) => callback.bind(this)(req, res, next))
  }

  // Helper for TS types
  protected getUserConnection(req: Request): Connection {
    if (!req.conn) throw new Error('No connection on Request')
    return req.conn
  }

}
