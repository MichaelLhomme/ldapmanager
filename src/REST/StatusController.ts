import restify from 'restify'

import { configInt } from 'Config'
import logger from 'Logger'

import { ConnectionPool } from 'LDAP/Connections'

import Controller from './Controller'


/**
 * Interface describing the server status
 */
export interface Status {
  status: string,
  time: Date,
  connected: boolean,
  connections: number,
  openedConnections: number
}


/**
 * A controller for general status information
 */
export default class StatusController extends Controller {

  /*
   * Create a new StatusController instance
   */
  constructor(server: restify.Server, pool: ConnectionPool) {
    super(server, pool)

    // Register routes
    this.get('/status', this.status)

    // Log status at regular interval
    const statusInterval = configInt('APP_STATUS_INTERVAL', 5 * 60 * 1000)
    if (statusInterval > 0) setInterval(() => logger.info({ msg: 'Server status', ...this.getStatus() }), statusInterval)
  }


  /*
   * Get the server status
   */
  private status(req: restify.Request, res: restify.Response, next: restify.Next): void {
    res.send(this.getStatus())
    next()
  }


  /*
   * Compute and return a Status object
   */
  private getStatus(): Status {
    return {
      status: 'running',
      time: new Date(),
      connected: this.pool.getAdminConnection().isConnected(),
      connections: this.pool.getConnectionCount(),
      openedConnections: this.pool.getOpenedConnectionCount()
    }
  }

}
