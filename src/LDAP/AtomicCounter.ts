import { createLogger, Logger } from 'Logger'

import { Connection } from 'LDAP/Connections'
import { Query, ChangeSet } from 'LDAP/Requests'


/**
 * Atomic counter implementation
 *
 * https://rexconsulting.net/2011/11/28/ldap-protocol-uidnumber-html/
 */
export default class AtomicCounter {

  // DN of the counter object
  private dn: string

  // Number of retries of the operation
  private retries: number

  // Internal logger instance
  private logger: Logger


  // Constructor
  public constructor(dn: string) {
    this.dn = dn
    this.retries = 3
    this.logger = createLogger({ source: 'AtomicCounter', dn })
  }


  // Read the current value of the counter
  private async readNextUid(conn: Connection): Promise<number> {
    const q: Query<string> = new Query.Builder((item: Query.Item) => item.uidNumber)
      .searchDN(this.dn)
      .objectClass('uidNext')
      .recurse(false)
      .build()

    const res: string[] = await q.search(conn)
    if (res.length !== 1) throw 'error computing uid, only one result expected'
    return parseInt(res[0])
  }


  /*
   * Retrieve the next available uid
   *
   * Operation is atomic, will fail and retry when needed
   */
  public async getNextUid(conn: Connection): Promise<number> {
    for (let i = 0; i < this.retries; i++) {
      const uidNext: number = await this.readNextUid(conn) // eslint-disable-line no-await-in-loop

      try {
        // tries to update using atomic operations, will fail if someone else got the uid
        await new ChangeSet() // eslint-disable-line no-await-in-loop
          .delete('uidNumber', uidNext)
          .add('uidNumber', uidNext + 1)
          .execute(conn, this.dn)

        return uidNext
      } catch (err) {
        this.logger.warn(`nextUid ${uidNext} was used by someone else, retrying... ${this.retries - i} retries left`)
      }
    }

    throw 'unable to get next uid'
  }

}
