/* eslint-disable max-classes-per-file */

import ldap, { Client } from 'ldapjs'

import { createLogger } from 'Logger'
import { config, configInt } from 'Config'


/*
 * Interface for connection options
 */
export interface ConnectionOptions {
  idleTimeout?: number
  reconnect?: boolean
}


/*
 * LDAP connection error exception
 */
export class ConnectionError extends Error {
  private originalError: Error | null

  constructor(msg: string, err: Error | null) {
    super(msg)
    this.originalError = err
  }
}


/*
 * An LDAP connection
 */
export default abstract class Connection {

  // LDAP connection string, extracted from config LDAP_URI
  private ldapUri: string

  // LDAP requests timeout, extracted from config LDAP_TIMEOUT
  private ldapTimeout: number

  // User DN, stored for reconnect
  private userDN: string

  // User password, stored for reconnect
  private userPass: string

  // Options for socket, stored for reconnect
  private options: ConnectionOptions | undefined

  // Internal instance of LDAP connection
  private client: Client | null

  // Flag for connection status
  private connected: boolean

  // Internal timestamp (used for cleanup)
  private disconnectedSince: number | null

  // Internal child logger instance with connection metadata
  protected log: any // eslint-disable-line @typescript-eslint/no-explicit-any


  /**
   * Create a new connection object
   */
  constructor(userDN: string, userPass: string, options?: ConnectionOptions) {
    this.ldapUri = config('LDAP_URI', 'ldap://localhost:389')
    this.ldapTimeout = configInt('LDAP_TIMEOUT', 10000)

    this.userDN = userDN
    this.userPass = userPass
    this.options = options

    this.client = null
    this.connected = false
    this.disconnectedSince = null

    this.log = createLogger({ base: 'Connection', instance: userDN })
  }


  /**
   * UserDN getter
   */
  public getUserDN(): string { return this.userDN }


  /**
   * Test if the socket is connected
   */
  public isConnected(): boolean { return this.connected }


  /**
   * Get the timestamp of the close event
   */
  public getDisconnectedSince(): number { return this.disconnectedSince || 0 }


  /**
   * Client getter
   */
  public getClient(): Client {
    if (!this.client) throw new Error('socket disconnected')
    return this.client
  }


  /**
   * Close the socket
   */
  public close(): void {
    if (this.client) this.client.unbind()
    this.client = null
    this.connected = false
    this.disconnectedSince = new Date().getTime()
  }


  /**
   * Connect the socket
   */
  public async connect() : Promise<void> {
    this.log.info(`Connecting to ${this.ldapUri}...`)

    try {
      this.disconnectedSince = null

      const client: Client = ldap.createClient({
        url: [this.ldapUri],
        timeout: this.ldapTimeout,
        ...this.options
      })

      // Handle connection and throw on errors
      this.client = await new Promise((resolve, reject) => {

        // Register handlers
        this.registerDebugHandlers(client)
        this.onClientCreated(client)

        // Handle disconnect events
        this.registerHandlers(client, ['end', 'close'], (_t: string, _err: Error | null) => {
          this.connected = false
        })

        // Handle connection errors
        this.registerHandlers(client, ['timeout', 'connectError', 'connectRefused', 'connectTimeout'], (t: string, err: Error | null) => {
          reject(new ConnectionError('Error registering handlers', err))
        })

        // Handle successful connect
        client.on('connect', async () => {
          this.log.debug(`Successfully connected to ${this.ldapUri}`)

          // Attempt to elevate with TLS
          this.log.debug('Attempting TLS negociation...')
          client.starttls({
            rejectUnauthorized: false,
          }, [], async (err: Error | null) => {
            if (err) {
              reject(new ConnectionError('TLS negociation error', err))
            } else {
              this.log.debug('Socket encrypted !')

              // Bind the user
              this.log.debug(`Connecting using user '${this.userDN}'`)
              client.bind(this.userDN, this.userPass, (errBind: Error | null) => {
                if (errBind) {
                  reject(new ConnectionError('Login error', errBind))
                } else {
                  this.log.info('Socket connected and authenticated !')
                  this.connected = true
                  resolve(client)
                }
              })
            }
          })
        })
      })
    } catch (err) {
      this.log.warn({ msg: 'Connection error', err })
      throw err
    }
  }


  /**
   * Overridable hook for registering socket handlers
   */
  protected abstract onClientCreated(client: Client): void


  /**
   * Helper to mass-register handlers
   */
  protected registerHandlers(
    client: Client,
    types: string[],
    handler: (type: string, err: Error | null) => void
  ): void {
    types.forEach(t => {
      client.on(t, (err: Error | null) => handler(t, err))
    })
  }


  /**
   * Register error handlers for debug
   */
  private registerDebugHandlers(client: Client) : void {
    const errors = ['error', 'setupError', 'socketTimeout', 'timeout', 'destroy']
    this.registerHandlers(client, errors, (t: string, err: Error | null) => {
      this.log.debug({ msg: `Connection received unknown event '${t}'`, err })
    })
  }

}
