import restify from 'restify'

import { config } from 'Config'
import { createLogger } from 'Logger'

import AtomicCounter from 'LDAP/AtomicCounter'
import { Connection, ConnectionPool } from 'LDAP/Connections'
import { Query, ldapCreate, ldapDelete } from 'LDAP/Requests'
import { User } from 'DTO/User'

import Controller, { Request } from './Controller'

const log = createLogger({ source: 'UserController' })


/**
 * Interface describing the query for users filtering
 */
export interface UserQuery {
  username?: string
  searchDN?: string
}


/**
 * Interface describing the query for user creation
 */
export interface UserCreate {
  username: string,
  password: string,
  firstName: string,
  lastName: string,
  uid: number,
  gid: number,
  home: string,
  email: string,
  sshKey: string
}


/**
 * Users controller
 */
export default class UserController extends Controller {

  // Internal AtomicCounter used to generate UIDs
  private uidCounter: AtomicCounter


  /**
   * Create a new UserController instance
   */
  constructor(server: restify.Server, pool: ConnectionPool) {
    super(server, pool)

    this.uidCounter = new AtomicCounter(config('APP_UID_COUNTER_DN'))

    this.get('/users', this.searchUsers)
    this.post('/users', this.createUser)
    this.get('/users/:userDN', this.getUser)
    this.put('/users/:userDN', this.updateUser)
    this.del('/users/:userDN', this.deleteUser)
  }


  /**
   * Handle search
   *
   * Search parameters are defined in interface `UserQuery`
   */
  private async searchUsers(req: Request, res: restify.Response, next: restify.Next): Promise<void> {
    try {
      // Process query parameters into filters
      const q = new Query.Builder((item: Query.Item) => User.fromLDAP(item))
        .objectClass('posixAccount')

      const searchParams: UserQuery = req.params
      if (searchParams.username) q.attr('uid', `*${searchParams.username}*`)

      if (searchParams.searchDN) q.searchDN(searchParams.searchDN)
      else q.searchDN('ou=users', true)

      // Execute the query
      const conn: Connection = this.getUserConnection(req)
      const searchResults: User[] = await q.build().search(conn)
      res.send(searchResults)
      next()
    } catch (err) {
      log.error({ msg: 'Error searching for users', err })
      next(new Error(`Error searching for users : ${err}`))
    }
  }


  /**
   * Handle get user
   */
  private async getUser(req: Request, res: restify.Response, next: restify.Next): Promise<void> {
    try {
      const { userDN } = req.params
      if (!userDN || userDN.length === 0) throw 'missing DN'

      // Execute the search request
      const q: Query<User> = new Query.Builder((item: Query.Item) => User.fromLDAP(item))
        .searchDN(userDN)
        .objectClass('posixAccount')
        .build()

      const conn: Connection = this.getUserConnection(req)
      const searchResults: User[] = await q.search(conn)
      if (searchResults.length !== 1) throw `error, only one user expected, got ${searchResults.length}`
      res.send(searchResults[0])
      next()
    } catch (err) {
      log.error({ msg: 'Error getting user', err })
      next(new Error(`Error getting user: ${err}`))
    }
  }


  /**
   * Handle update user
   */
  private async updateUser(req: Request, res: restify.Response, next: restify.Next): Promise<void> {
    try {
      const { userDN } = req.params
      if (!userDN || userDN.length === 0) throw 'missing DN'

      // Extract request body
      const {
        firstName, lastName, email, password, sshKey
      }:
      {
        firstName: string | undefined,
        lastName: string | undefined,
        email: string | undefined,
        password: string | undefined
        sshKey: string | undefined
      } = req.body

      const updater = User.update()
      if (firstName && lastName) updater.setNames(firstName, lastName)
      if (email) updater.setEmail(email)
      if (password) updater.setPassword(password)
      if (sshKey !== undefined) updater.setSSHKey(sshKey)

      // Execute the search request
      const conn: Connection = this.getUserConnection(req)
      await updater.execute(conn, userDN)

      log.info(`User '${userDN}' updated`)
      res.send(updater.getUpdates())
      next()
    } catch (err) {
      log.error({ msg: 'Error updating user', err })
      next(new Error(`Error updating user: ${err}`))
    }
  }


  /**
   * Handle create
   */
  private async createUser(req: Request, res: restify.Response, next: restify.Next): Promise<void> {
    try {
      // Validate and extract query data
      if (!req.params.parent) throw 'missing "parent" property'
      const { parent }: { parent: string } = req.params

      const user: User = User.fromQuery(req.params)
      const fullDN = `uid=${user.username},${parent}`

      // Generate new uid
      const conn: Connection = this.getUserConnection(req)
      // TODO check first if already exists to prevent generating unused UIDs
      user.uid = await this.uidCounter.getNextUid(conn)

      // Execute the ldap request
      await ldapCreate(conn, fullDN, user.toLDAP())

      log.info(`New user '${user.username}' created`)
      res.send({ ...user, password: undefined })
      next()
    } catch (err) {
      log.error({ msg: 'Error creating user', err })
      next(new Error(`Error creating user: ${err}`))
    }
  }


  /**
   * Handle delete
   */
  private async deleteUser(req: Request, res: restify.Response, next: restify.Next): Promise<void> {
    try {
      // Validate and extract query data
      const { userDN } = req.params
      if (!userDN || userDN.length === 0) throw 'missing user DN'

      // Execute the delete request
      const conn: Connection = this.getUserConnection(req)
      await ldapDelete(conn, userDN)

      log.info(`User '${userDN}' deleted`)
      res.send()
      next()
    } catch (err) {
      log.error({ msg: 'Error deleting user', err })
      next(new Error(`Error deleting user: ${err}`))
    }
  }

}
