import { Client } from 'ldapjs'

import Connection, { ConnectionOptions } from './Connection'


/**
 * A persistent LDAP connection
 *
 * Will try to auto-reconnect
 */
export default class PersistentConnection extends Connection {

  /**
   * Create a new persistent connection
   */
  constructor(userDN: string, userPass: string, options?: ConnectionOptions) {
    const tmp: unknown = {
      ...options,
      reconnect: {
        initialDelay: 1000,
        maxDelay: 5000,
        failAfter: Infinity
      }
    }
    super(userDN, userPass, <ConnectionOptions>tmp)
  }


  /**
   * Does nothing
   */
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  protected onClientCreated(_client: Client): void { }

}
