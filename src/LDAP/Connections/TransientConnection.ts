import { Client } from 'ldapjs'

import { configInt } from 'Config'
import Connection, { ConnectionOptions } from './Connection'


/**
 * A transient LDAP connection
 *
 * Will close after APP_IDLE_TIMEOUT is reached
 */
export default class TransientConnection extends Connection {

  /**
   * Create a new transient connection
   */
  constructor(userDN: string, userPass: string, options?: ConnectionOptions) {
    super(userDN, userPass, {
      ...options,
      idleTimeout: configInt('APP_IDLE_TIMEOUT', 2 * 60 * 1000)
    })
  }


  /**
   * Override onClientCreated to register disconnection hooks
   */
  protected onClientCreated(client: Client): void {

    // Handle connection idling by closing
    this.registerHandlers(client, ['idle'], (_t: string, _err: Error | null) => {
      this.log.debug('Connection idling, will close')
      this.close()
    })

    // Handle idling and disconnect events
    this.registerHandlers(client, ['end', 'close'], (t: string, _err: Error | null) => {
      this.log.info(`Connection received '${t}'  cleaning...`)
      client.unbind()
    })

  }

}
