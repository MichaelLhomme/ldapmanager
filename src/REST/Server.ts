import restify from 'restify'
import corsMiddleware from 'restify-cors-middleware2'
import rjwt from 'restify-jwt-community'

import { createLogger } from 'Logger'
import { config, configInt } from 'Config'

import { ConnectionPool } from 'LDAP/Connections'

import { Request } from './Controller'
import StatusController from './StatusController'
import AuthController from './AuthController'
import UserController from './UserController'
import GroupController from './GroupController'


const log = createLogger({ base: 'REST/Server' })


/**
 * Class for managing REST HTTP server
 */
export default class Server {

  // Internal reference to the connection pool
  pool: ConnectionPool

  // Internal reference to the underlying Restify server
  server: restify.Server

  // Controllers
  statusController: StatusController

  authController: AuthController

  userController: UserController

  groupController: GroupController


  /**
   * Create a new RESTServer
   */
  constructor(pool: ConnectionPool) {
    this.pool = pool
    this.server = restify.createServer()

    this.initMiddlewares(this.server, pool)

    // Register REST controllers
    this.statusController = new StatusController(this.server, pool)
    this.authController = new AuthController(this.server, pool)
    this.userController = new UserController(this.server, pool)
    this.groupController = new GroupController(this.server, pool)
  }


  /**
   * Start the server
   */
  public start(): void {
    this.server.listen(configInt('HTTP_PORT', 8080), () => {
      log.info(`HTTP started and listening at ${this.server.url}`)
    })
  }


  /**
   * Init HTTP middlewares
   */
  private initMiddlewares(server: restify.Server, pool: ConnectionPool): void {
    const cors = corsMiddleware({
      preflightMaxAge: 5, // Optional
      origins: ['*'],
      allowHeaders: ['Authorization']
    })

    server.pre(cors.preflight)
    server.use(cors.actual)

    // Parse query parameters
    server.use(restify.plugins.queryParser({
      mapParams: true
    }));

    // Parse JSON bodies and URL params in requests
    server.use(restify.plugins.bodyParser({
      mapParams: true,
      requestBodyOnGet: true
    }))

    // Enforce authentication
    server.use(rjwt({ secret: config('JWT_SECRET') }).unless({
      path: ['/login']
    }));

    // Retrieve user's connection and reconnect it if needed
    server.use((req: Request, _res : restify.Response, next: restify.Next) => {
      // skip if there is no user payload
      if (!req.user) next()

      // else try to retrieve an existing connection and reconnect it if needed
      else {
        try {
          req.conn = pool.getUserConnection(req.user.dn)
          if (req.conn.isConnected()) next()
          else {
            req.conn.connect()
              .then(() => next())
              .catch((err: Error) => next(err))
          }
        } catch (err) {
          next(new Error(`${err}`))
        }
      }
    })

  }
}
