/* eslint-disable max-classes-per-file, no-use-before-define */

/**
 * Base class for LDAP Filters
 */
export abstract class Filter {

  // Serialize the filter into a string
  public abstract serialize(): string

}


/**
 * A simple filter for matching one attribute value
 */
export class SimpleFilter extends Filter {

  // The attribute name
  private attr: string

  // The attribute value
  private value: string

  // Create a simple filter instance
  constructor(attr?: string, value?: string) {
    super()
    if (!attr) throw new Error(`SimpleFilter cannot accept an undefined name (got '${attr}')`)
    if (!value) throw new Error(`SimpleFilter cannot accept an undefined value (got '${value}' for attr ${attr})`)

    this.attr = attr
    this.value = value
  }

  // Serialize the filter into a string
  public serialize(): string {
    return `(${this.attr}=${this.value})`
  }

}


/**
 * A NOT filter
 */
export class NotFilter extends Filter {

  // Reference to the inner negated test
  private innerFilter: Filter | null

  // Create a NotFilter instance
  constructor() {
    super()
    this.innerFilter = null
  }

  // Set the inner test as a simple filter for matching one attribute value
  public attr(attr: string, value?: string): NotFilter {
    this.innerFilter = new SimpleFilter(attr, value)
    return this
  }

  // Set the inner test as a complex AND filter
  public and(hook: (filter: AndFilter) => void): NotFilter {
    const filter: AndFilter = new AndFilter()
    hook(filter)
    this.innerFilter = filter
    return this
  }

  // Set the inner test as a complex OR filter
  public or(hook: (filter: OrFilter) => void): NotFilter {
    const filter: OrFilter = new OrFilter()
    hook(filter)
    this.innerFilter = filter
    return this
  }

  // Serialize the filter into a string
  public serialize(): string {
    if (!this.innerFilter) throw new Error('NotFilter missing inner filter')
    return `(!${this.innerFilter.serialize()})`
  }

}


/**
 * Base class for group filters
 */
export class GroupFilter {

  // The symbol used for the group filter (`&` or `|`)
  private symbol: string

  // An array of inner filters
  protected filters: Filter[]

  // Create a new group filter
  constructor(symbol: string, filters?: Filter[]) {
    this.symbol = symbol
    this.filters = filters || []
  }

  // Add another filter
  public addFilter(filter: Filter): GroupFilter {
    this.filters.push(filter)
    return this
  }

  // Merge another group filter
  public mergeFilters(groupFilter: GroupFilter): GroupFilter {
    this.filters = this.filters.concat(groupFilter.filters)
    return this
  }

  // Add an inner simple filter for matching one attribute value
  public attr(attr: string, value?: string): GroupFilter {
    this.addFilter(new SimpleFilter(attr, value))
    return this
  }

  // Add multiple simple filters for matching the attributes values
  public attrs(attrs: any): GroupFilter { // eslint-disable-line @typescript-eslint/no-explicit-any
    Object
      .keys(attrs)
      .map(attr => this.addFilter(new SimpleFilter(attr, attrs[attr])))
    return this
  }

  // Add and inner NOT filter
  public not(hook: (filter: NotFilter) => void): GroupFilter {
    const filter: NotFilter = new NotFilter()
    hook(filter)
    this.addFilter(filter)
    return this
  }

  // Add and inner AND filter
  public and(hook: (filter: AndFilter) => void): GroupFilter {
    const filter: AndFilter = new AndFilter()
    hook(filter)
    this.addFilter(filter)
    return this
  }

  // Add and inner OR filter
  public or(hook: (filter: OrFilter) => void): GroupFilter {
    const filter: OrFilter = new OrFilter()
    hook(filter)
    this.addFilter(filter)
    return this
  }

  // Serialize the filter into a string
  public serialize(): string {
    if (this.filters.length <= 0) throw new Error('GroupFilter must have some inner filters')
    return `(${this.symbol}${this.filters.map(f => f.serialize()).join('')})`
  }

}


/**
 * OR filter
 */
export class OrFilter extends GroupFilter {
  constructor(filters?: Filter[]) { super('|', filters) }
}


/**
 * AND filter
 */
export class AndFilter extends GroupFilter {
  constructor(filters?: Filter[]) { super('&', filters) }
}
