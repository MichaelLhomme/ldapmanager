/* Related to workaround for static inner class */
/* eslint-disable max-classes-per-file, import/export */

import { validate, EntityUpdater } from 'DTO/Helpers'

import { Query } from 'LDAP/Requests'
import { User as UserLDAP } from 'LDAP/Schema'
import { UserCreate } from 'REST/UserController'


/**
 * DTO object for users
 */
export class User {

  id?: string

  username?: string

  firstName?: string

  lastName?: string

  password?: string

  sshKey?: string

  home?: string

  email?: string

  uid?: number

  gid?: number


  /**
   * Returns a new User.Updater instance
   */
  public static update() { return new User.Updater() }


  /**
   * Serialize this user into its LDAP representation
   */
  public toLDAP(): UserLDAP {
    validate(this, ['username', 'email', 'firstName', 'lastName', 'uid', 'gid', 'home', 'password'])

    // The default values below are only there to quiet Typescript, validate ensure the fields are present
    const ldapUser: UserLDAP = {
      objectClass: ['inetOrgPerson', 'posixAccount', 'ldapPublicKey'],
      cn: this.username || '',
      sn: this.lastName || '',
      displayName: `${this.firstName} ${this.lastName}`,
      givenName: this.firstName || '',
      uid: this.username || '',
      uidNumber: this.uid || 1000,
      gidNumber: this.gid || 1000,
      mail: this.email || '',
      homeDirectory: this.home || '/home',
      userPassword: this.password,
      sshPublicKey: this.sshKey,
      loginShell: '/bin/bash'
    }

    // Manually clean optionnal values as ldapjs fails to handle `undefined` values
    if (!this.sshKey) delete ldapUser.sshPublicKey

    return ldapUser
  }


  /**
   * Create a new User by parsing a query body
   *
   * Throw errors on validation problems
   */
  public static fromQuery(body: UserCreate) {
    validate(body, ['username', 'firstName', 'lastName', 'password', 'email'])

    const user = new User()
    user.username = body.username
    user.firstName = body.firstName
    user.lastName = body.lastName
    user.password = body.password
    user.gid = 2000
    user.home = `/home/${body.username}`
    user.email = body.email
    user.sshKey = body.sshKey || undefined
    return user
  }


  /**
   * Create a new User by parsing a LDAP entry object
   */
  public static fromLDAP(obj: Query.Item): User {
    const ldapUser: UserLDAP = (obj as unknown) as UserLDAP
    validate(ldapUser, ['dn', 'uid', 'sn', 'uidNumber', 'gidNumber', 'homeDirectory', 'mail'])

    const user = new User()
    user.id = ldapUser.dn
    user.username = ldapUser.uid
    user.firstName = ldapUser.givenName
    user.lastName = ldapUser.sn
    user.uid = ldapUser.uidNumber
    user.gid = ldapUser.gidNumber
    user.home = ldapUser.homeDirectory
    user.email = ldapUser.mail
    user.sshKey = ldapUser.sshPublicKey || undefined
    return user
  }


  /**
   * Static helper to update users
   */
  public static Updater = class extends EntityUpdater {

    public setNames(firstName: string, lastName: string): User.Updater {
      this.cs
        .replace('givenName', firstName)
        .replace('sn', lastName)
        .replace('displayName', `${firstName} ${lastName}`)

      this.updates = { ...this.updates, firstName, lastName }
      return this
    }

    public setEmail(email: string): User.Updater {
      this.cs.replace('mail', email)
      this.updates = { ...this.updates, email }
      return this
    }

    public setPassword(password: string): User.Updater {
      this.cs.replace('userPassword', password)
      // DO NOT return password in updates
      return this
    }

    public removeSSHKey(): User.Updater {
      this.cs.delete('sshPublicKey')
      this.updates = { ...this.updates, sshKey: null }
      return this
    }

    public setSSHKey(sshKey: string | null): User.Updater {
      if (sshKey === null) return this.removeSSHKey()

      this.cs.replace('sshPublicKey', sshKey)
      this.updates = { ...this.updates, sshKey }
      return this
    }

  }

}


// Workaround for inner class type access
// eslint-disable-next-line @typescript-eslint/no-namespace
export namespace User {
  export type Updater = typeof User.Updater.prototype
}
