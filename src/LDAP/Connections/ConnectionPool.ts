import { config, configInt } from 'Config'
import { createLogger } from 'Logger'

import Connection from './Connection'
import PersistentConnection from './PersistentConnection'
import TransientConnection from './TransientConnection'

const log = createLogger({ base: 'ConnectionPool' })


/**
 * A pool manager for LDAP connections
 */
export default class ConnectionPool {

  // Main admin connection with keepAlive
  private adminConnection: PersistentConnection

  // A map of currently active user connections
  private userConnections: Map<string, TransientConnection>


  /**
   * Create a new pool manager instance
   */
  constructor() {
    log.info('Starting ConnectionPool...')
    this.userConnections = new Map()
    this.adminConnection = ConnectionPool.createAdminConnection()
    this.adminConnection.connect()
      .then(() => log.info('Admin socket connected'))
      .catch(err => log.error(`Unable to connect admin socket: ${err.message}`))

    // TODO remove and use autoReconnect on the ldap client
    // this.autoReconnect()
    // setInterval(() => this.autoReconnect(), 1000)

    setInterval(() => this.cleanup(), 3000)
  }


  /**
   * Retrieve the admin connection
   */
  public getAdminConnection(): Connection { return this.adminConnection }


  /**
   * Get the number of active connection (regardless of connection status)
   */
  public getConnectionCount(): number { return this.userConnections.size }


  /**
   * Get the number of currently connected connection
   */
  public getOpenedConnectionCount(): number {
    return Array.from(this.userConnections.values()).filter(c => c.isConnected()).length
  }


  /**
   * Retrieve a connection for the user identified by the given LDAP DN
   */
  public getUserConnection(dn: string): Connection {
    const conn = this.userConnections.get(dn)
    if (!conn) throw new Error(`No active connection for user '${dn}'`)
    return conn
  }


  /**
   * Remove the connection associated (if any) to the given LDAP DN
   */
  public removeUserConnection(userDN: string): void {
    log.info(`Removing connection for user '${userDN}'...`)
    const conn: Connection = this.getUserConnection(userDN)
    conn.close()
    this.userConnections.delete(userDN)
  }


  /**
   * Create a new connection and connect it using the given credentials
   */
  public async createUserConnection(userDN: string, userPass: string): Promise<Connection> {
    log.info(`Creating connection for user '${userDN}'...`)

    // Close existing user socket if present
    const existingConn: Connection | undefined = this.userConnections.get(userDN)
    if (existingConn) existingConn.close()

    // Create and connect a new user connection
    const conn: TransientConnection = new TransientConnection(userDN, userPass)
    await conn.connect()
    this.userConnections.set(userDN, conn)

    return conn
  }


  /**
   * Create a new connection and connect it using the administrator credentials
   */
  private static createAdminConnection(): PersistentConnection {
    const adminDN: string = config('LDAP_ADMIN_DN')
    const adminPass: string = config('LDAP_ADMIN_PASSWORD')

    log.info(`Creating admin connection for user '${adminDN}'...`)
    return new PersistentConnection(adminDN, adminPass)
  }


  /**
   * Clean closed connection after APP_DISCARD_TIMEOUT is elapsed
   */
  private cleanup(): void {
    const discardTimeout = configInt('APP_DISCARD_TIMEOUT', 5 * 60 * 1000)
    const currentTimestamp: number = new Date().getTime()

    Array.from(this.userConnections.values())
      .filter(c => !c.isConnected() && (currentTimestamp - c.getDisconnectedSince() > discardTimeout))
      .forEach(c => {
        const userDN: string = c.getUserDN()
        this.removeUserConnection(userDN)
      })
  }


  // TODO remove this and use autoReconnect on the admin socket
  private async autoReconnect(): Promise<void> {
    if (!this.adminConnection.isConnected()) {
      log.warn('Admin socket disconnected, trying to reconnect...')
      this.adminConnection.connect()
        .then(() => log.info('Admin socket reconnected'))
        .catch(err => log.error(`Unable to reconnect admin socket: ${err.message}`))
    }
  }


}
