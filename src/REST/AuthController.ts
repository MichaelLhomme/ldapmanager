import jwt from 'jsonwebtoken'
import restify from 'restify'

import { config } from 'Config'
import { createLogger } from 'Logger'

import { Base as BaseLDAP } from 'LDAP/Schema'
import { Query } from 'LDAP/Requests'
import { Connection, ConnectionPool } from 'LDAP/Connections'

import Controller from './Controller'

const log = createLogger({ base: 'REST/AuthController' })


/**
 * Interface to describe account with operational attributes, used for login
 */
interface AccountInfo extends BaseLDAP {
  dn: string
  memberOf: string | string[]
}


/**
 * Authentication controller
 */
export default class AuthController extends Controller {

  // JWT secret key
  jwtSecret: string


  /**
   * Create a new AuthController instance
   */
  constructor(server: restify.Server, pool: ConnectionPool) {
    super(server, pool)
    this.jwtSecret = config('JWT_SECRET')

    this.post('/logout', this.logout)
    this.post('/login', this.login)
  }


  /**
   * Handle logout
   */
  private logout(req: restify.Request, res: restify.Response, next: restify.Next): void {
    try {
      const conn: Connection = this.getUserConnection(req)
      conn.close()
      this.pool.removeUserConnection(conn.getUserDN())

      res.send()
      next()
    } catch (err) {
      log.error({ msg: 'Error searching for users', err })
      next(new Error(`Error searching for users : ${err}`))
    }
  }


  /**
   * Get account information for a given username
   */
  private async findUser(conn: Connection, username: string): Promise<AccountInfo> {
    const q: Query<AccountInfo> = new Query.Builder((item: Query.Item) => item)
      .select(['dn', '+']) // select operational attributes
      .objectClass('inetOrgPerson') // don't restrict to posixAccount in order to allow connection from LDAP system users
      .attr('uid', username)
      .build()

    const searchResults: AccountInfo[] = await q.search(conn)
    if (searchResults.length === 0) throw new Error('No such user')
    if (searchResults.length !== 1) throw new Error(`Login failed, only one user expected, got ${searchResults.length}`)

    return searchResults[0]
  }


  /**
   * Process groups as roles
   */
  private getRoles(user: AccountInfo): string[] {
    const roles: string[] = []

    // eslint-disable-next-line no-nested-ternary
    const groups: string[] = user.memberOf
      ? Array.isArray(user.memberOf) ? user.memberOf : [user.memberOf]
      : []

    groups.forEach(group => {
      if (group === config('APP_GROUP_ADMINISTRATORS')) roles.push('administrators')
      if (group === config('APP_GROUP_MANAGERS')) roles.push('managers')
      if (group === config('APP_GROUP_USERS')) roles.push('users')
    })

    return roles
  }


  /**
   * Handle login
   */
  // eslint-disable-next-line max-len
  private async login(req: restify.Request, res: restify.Response, next: restify.Next): Promise<void> {
    try {
      // Extract and validate parameters
      const { username, password } = req.body
      if (!username || !password) throw new Error('Missing parameters for login')

      // Find user in LDAP directory
      const conn: Connection = this.pool.getAdminConnection()
      const user = await this.findUser(conn, username)
      const roles = this.getRoles(user)

      // Try to create a dedicated connection then returns a JWT token
      await this.pool.createUserConnection(user.dn, password)
      const token = jwt.sign({ dn: user.dn, roles }, this.jwtSecret, {
        expiresIn: config('JWT_TIMEOUT', '15m') // token expires in 15 minutes
      })

      res.send({ token })
      next()
    } catch (err) {
      log.error({ msg: 'Error searching for user', err })
      next(new Error(`Error searching for user: ${err}`))
    }
  }

}
