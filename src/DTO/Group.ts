/* Related to workaround for static inner class */
/* eslint-disable max-classes-per-file, import/export */

import { EntityUpdater } from 'DTO/Helpers'

import { Query } from 'LDAP/Requests'
import { OU as OULDAP } from 'LDAP/Schema'
import { GroupCreate } from 'REST/GroupController'


/**
 * DTO object for groups
 */
export class Group {

  id?: string

  name?: string

  description?: string


  /**
   * Returns a new Group.Updater instance
   */
  public static update() { return new Group.Updater() }


  /**
   * Serialize this group into its LDAP representation
   */
  public toLDAP(): OULDAP {
    const ldapGroup: OULDAP = { objectClass: 'organizationalUnit' }
    ldapGroup.ou = this.name
    if (this.description) ldapGroup.description = this.description
    return ldapGroup
  }


  /**
   * Create a new Group by parsing a query body
   *
   * Throw errors on validation problems
   */
  public static fromQuery(body: GroupCreate) {
    if (!body.name) throw new Error('Missing parameters for create')

    const group: Group = new Group()
    group.name = body.name
    group.description = body.description || undefined
    return group
  }


  /**
   * Create a new Group by parsing a LDAP entry object
   */
  public static fromLDAP(obj: Query.Item): Group {
    const ldapOU: OULDAP = obj

    const group = new Group()
    group.id = ldapOU.dn
    group.name = ldapOU.ou
    group.description = ldapOU.description || undefined
    return group
  }


  /**
   * Static helper to update users
   */
  public static Updater = class extends EntityUpdater {

    public setDescription(description: string): Group.Updater {
      this.cs.replace('description', description)
      this.updates = { ...this.updates, description }
      return this
    }

  }

}


// Workaround for inner class type access
// eslint-disable-next-line @typescript-eslint/no-namespace
export namespace Group {
  export type Updater = typeof Group.Updater.prototype
}
