import Logger from 'Logger'

import { Connection } from 'LDAP/Connections'
import { Base as LDAPBase } from 'LDAP/Schema'
import {
  Change, SearchEntry, SearchEntryObject, SearchCallbackResponse
} from 'ldapjs'


/**
 * LDAP search scope
 *
 * https://ldapwiki.com/wiki/LDAP%20Search%20Scopes
 */
export enum SearchScope { // eslint-disable-line no-shadow
  Sub = 'sub',
  Base = 'base'
}


/**
 * LDAP search query
 */
export function ldapSearch<T>(
  conn: Connection,
  dn: string,
  scope: SearchScope,
  // TODO fix the type
  options: any, // eslint-disable-line @typescript-eslint/no-explicit-any
  mapper: (entry: SearchEntryObject) => T
): Promise<T[]> {
  return new Promise<T[]>((resolve, reject) => {
    const results: T[] = []
    conn.getClient().search(
      dn,
      { scope: scope as string, ...options },
      (err: Error | null, res: SearchCallbackResponse) => {
        res.on('searchEntry', (entry : SearchEntry) => {
          try {
            results.push(mapper(entry.object))
          } catch (mapError) {
            Logger.warn({
              source: 'ldapSearch', msg: 'Error reading search entry, skipping', mapError, entry: entry.object
            })
          }
        })
        res.on('error', (localErr: Error | null) => reject(localErr))
        res.on('end', () => resolve(results))
      }
    )
  })
}


/**
 * LDAP create query
 */
export function ldapCreate<T extends LDAPBase>(
  conn: Connection,
  dn: string,
  data: T
): Promise<T> {
  return new Promise((resolve, reject) => {
    conn.getClient().add(dn, data, (err: Error | null) => {
      if (err) reject(err)
      else resolve(data)
    })
  })
}


/**
 * LDAP update query
 */
export function ldapModify(
  conn: Connection,
  dn: string,
  changes: Change[]
): Promise<void> {
  return new Promise((resolve, reject) => {
    conn.getClient().modify(dn, changes, (err: Error | null) => {
      if (err) reject(err)
      else resolve()
    })
  })
}


/**
 * Delete LDAP objects
 */
export function ldapDelete(
  conn: Connection,
  dn: string,
): Promise<void> {
  return new Promise((resolve, reject) => {
    conn.getClient().del(dn, (err: Error | null) => {
      if (err) reject(err)
      else resolve()
    })
  })
}
