/**
 * Base interface for every LDAP object
 */
export interface Base {
  dn?: string
  objectClass?: string | string[]
}


/**
 * Mapping for the 'ou' schema
 */
export interface OU extends Base {
  ou?: string
  description?: string
}


/**
 * Mapping for the posixAccount schema
 */
export interface posixAccount extends Base {
  cn: string
  uid: string
  uidNumber: number
  gidNumber: number
  homeDirectory: string
  loginShell?: string
}


/**
 * Mapping for the 'inetOrgPerson' schema
 */
export interface inetOrgPerson extends Base {
  cn: string
  sn: string
  userPassword?: string
  givenName?: string
  displayName?: string
  mail?: string
  jpegPhoto?: string
}


/**
 * Mapping for custom schema 'ldapPublicKey'
 */
export interface ldapPublicKey extends Base {
  sshPublicKey?: string
}


/**
 * Mapping for a complete user schema
 */
export interface User extends inetOrgPerson, posixAccount, ldapPublicKey {
}
