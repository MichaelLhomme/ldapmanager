/* Related to workaround for static inner class */
/* eslint-disable max-classes-per-file, import/export, no-use-before-define */

import { SearchEntryObject } from 'ldapjs'

import { config } from 'Config'
import { createLogger, Logger } from 'Logger'

import { Connection } from 'LDAP/Connections'

import { SearchScope, ldapSearch } from './Primitives'
import {
  Filter,
  GroupFilter,
  AndFilter,
  OrFilter,
  NotFilter,
  SimpleFilter
} from './Filter'


// Logger instance
const logger: Logger = createLogger({ source: 'Query' })


/**
 * A class for generating LDAP search queries
 */
export class Query<T> {

  // The base DN for the search
  private searchDN?: string

  // LDAP search options
  private options?: any // eslint-disable-line @typescript-eslint/no-explicit-any

  // Flag for recursive search
  private scope: SearchScope

  // LDAP attributes to select
  private attributes?: string[]

  // LDAP Filter string
  private filter?: string

  // Callback to map from LDAP entry to custom type
  private mapper: (entry: Query.Item) => T


  /**
   * Create a new query instance
   */
  private constructor(mapper: (entry: Query.Item) => T) {
    this.mapper = mapper
    this.scope = SearchScope.Sub
  }


  /**
   * Execute the search
   */
  public search(conn: Connection): Promise<T[]> {
    const rootDN: string = config('LDAP_ROOT_DN')
    const fullSearchDN: string = this.searchDN && this.searchDN.length > 0 ? this.searchDN : rootDN

    const filter: string | undefined = this.filter // eslint-disable-line prefer-destructuring
    const { attributes } = this
    const options = { ...this.options, filter, attributes }

    logger.debug({
      msg: 'Executing LDAP search', scope: this.scope, searchDN: fullSearchDN, ...options
    })
    return ldapSearch(conn, fullSearchDN, this.scope, options, this.mapper)
  }


  /**
   * Factory class for LDAP queries
   */
  public static Builder: any = class <U> { // eslint-disable-line @typescript-eslint/no-explicit-any

    // Buffered query
    private q: Query<U>

    // Buffered filter
    private filter?: Filter

    // Create a new Query Builder instance
    constructor(mapper: (entry: Query.Item) => U) {
      this.q = new Query(mapper)
    }

    // Returns the accumulated Query
    public build(): Query<U> {
      this.q.filter = this.filter ? this.filter.serialize() : undefined
      return this.q
    }

    // Update the scope of the query to set/unset recursivity
    public recurse(recurse: boolean): Query.Builder {
      this.q.scope = recurse ? SearchScope.Sub : SearchScope.Base
      return this
    }

    // Set the base DN for the search
    public searchDN(dn: string | undefined, relative?: boolean): Query.Builder {
      if (relative) {
        const rootDN: string = config('LDAP_ROOT_DN')
        this.q.searchDN = `${dn},${rootDN}`
      } else {
        this.q.searchDN = dn
      }

      return this
    }

    // Set the attributes to select
    public select(attributes: string[]): Query.Builder {
      this.q.attributes = attributes
      return this
    }

    // Helper to stack filters on the fly
    private addFilter(filter: Filter): Query.Builder {
      if (!this.filter) this.filter = filter
      else if (this.filter instanceof AndFilter) this.filter.addFilter(filter)
      else this.filter = new AndFilter([this.filter, filter])
    }

    // Helper to stack filters on the fly
    private mergeFilters(filter: GroupFilter): Query.Builder {
      if (!this.filter) this.filter = filter
      else if (this.filter instanceof AndFilter) this.filter.mergeFilters(filter)
      else if (filter instanceof AndFilter) this.filter = new AndFilter([this.filter]).mergeFilters(filter)
      else this.filter = new AndFilter([this.filter, filter])
    }


    // Set a simple filter to match an attribute value
    public attr(attr: string, value: string): Query.Builder {
      this.addFilter(new SimpleFilter(attr, value))
      return this
    }

    // Helper to quickly set an AndFilter matching the given attributes values
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    public attrs(attrs: any): Query.Builder {
      this.mergeFilters(new AndFilter().attrs(attrs))
      return this
    }

    // Helper to quickly set a simple filter on the objectClass
    public objectClass(oc: string): Query.Builder {
      return this.attr('objectClass', oc)
    }

    // Set a NotFilter
    public not(hook: (filter: NotFilter) => void): Query.Builder {
      const filterBuilder: NotFilter = new NotFilter()
      hook(filterBuilder)
      this.addFilter(filterBuilder)
      return this
    }

    // Set an AndFilter
    public and(hook: (filter: AndFilter) => void): Query.Builder {
      const filterBuilder: AndFilter = new AndFilter()
      hook(filterBuilder)
      this.mergeFilters(filterBuilder)
      return this
    }

    // Set an OrFilter
    public or(hook: (filter: OrFilter) => void): Query.Builder {
      const filterBuilder: OrFilter = new OrFilter()
      hook(filterBuilder)
      this.addFilter(filterBuilder)
      return this
    }

  }

}

// Workaround for inner class type access
// eslint-disable-next-line @typescript-eslint/no-namespace
export namespace Query {
  export type Builder = typeof Query.Builder.prototype
  export type Item = SearchEntryObject
}
