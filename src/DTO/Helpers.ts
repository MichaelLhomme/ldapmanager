/* eslint-disable @typescript-eslint/no-explicit-any */

import { Connection } from 'LDAP/Connections'
import { ChangeSet } from 'LDAP/Requests'


/**
 * Helper function to validate the presence of fields in an object
 *
 * Throws an exception when fields are missing
 */
export function validate(obj: Record<string, any>, fields: string[]) {
  const errors: string[] = fields.flatMap(field => {
    if (!obj[field]) return [`'${field}'`]
    return []
  })

  if (errors.length > 0) throw `Object is missing the following fields: ${errors.join(',')}`
}


/**
 * Base class for entity updaters
 *
 * - Provide a buffered ChangeSet and methods to help chaining the setters
 * - Accumulates updates for REST responses
 */
export class EntityUpdater {

  // Buffered ChangeSet
  protected cs: ChangeSet

  // Buffered updates for REST query result
  protected updates: Record<string, any>


  public constructor() {
    this.cs = new ChangeSet()
    this.updates = {}
  }


  // Get updates as a REST response
  public getUpdates(): Record<string, any> { return this.updates }

  // Execute the underlying ChangeSet
  public async execute(conn: Connection, dn: string): Promise<void> {
    await this.cs.execute(conn, dn)
  }

}
